package my.home.work.algoritms;

/**
 * Created by batman on 12.10.2016.
 */
public class Vijener {
    private final static String ALPHABET = "абвгдежзийклмнопрстуфхцчшщъыьэюя0123456789 ,./?';:{}[]_-+=!";
    private final static String[] alphabetTable=generate();
    private static String[] generate() {
        String alphabetTable[] = new String[ALPHABET.length()];
        StringBuilder strB = new StringBuilder();
        for (int i = 0; i < ALPHABET.length(); i++) {
            for (int j = 0; j < ALPHABET.length(); j++) {
                strB.append(ALPHABET.charAt((j + i) % ALPHABET.length()));
            }
            System.out.println(strB.toString());
            alphabetTable[i] = strB.toString();
            strB.delete(0, strB.length());

        }
        return alphabetTable;
    }

    public static String encrypt(String message, String key) {
       // String[] alphabetTable = generate();
        message = message.toLowerCase();
        String res = null;
        StringBuilder strB = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            for (int j = 0; j < ALPHABET.length(); j++) {
                if (key.charAt(i % key.length()) == alphabetTable[j].charAt(0)) {
                    int k=0;
                    while(ALPHABET.charAt(k)!=message.charAt(i)){
                        k++;
                    }
                    strB.append(alphabetTable[j].charAt(k));
                }


            }

        }
        res = strB.toString();
        return res;
    }
    public static String decryption(String message, String key){
        String res=null;
        StringBuilder strB = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            for (int j = 0; j < ALPHABET.length(); j++) {
                if (key.charAt(i % key.length()) == alphabetTable[j].charAt(0)) {
                    int k=0;
                    while(alphabetTable[j].charAt(k)!=message.charAt(i)){
                        k++;
                    }
                    strB.append(ALPHABET.charAt(k));
                }


            }

        }
        res = strB.toString();
        return res;
    }
}
