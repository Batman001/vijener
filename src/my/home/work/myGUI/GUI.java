package my.home.work.myGUI;


import my.home.work.algoritms.Vijener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by batman on 08.10.2016.
 */
public class GUI {
    private JPanel panel1;
    private JTextField outputText;
    private JTextField inputText;
    private JRadioButton flag;
    private JTextField key;
    private JButton ok;

    public JPanel getPanel1() {
        return panel1;
    }

    public void setPanel1(JPanel panel1) {
        this.panel1 = panel1;
    }



    public GUI() {

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String message=inputText.getText();
                String k=key.getText();
                boolean f=flag.isSelected();
                if(f==true) {
                    outputText.setText(Vijener.encrypt(message,k));
                }else{
                    outputText.setText(Vijener.decryption(message,k));
                }

            }
        });
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }


}
